import "@/styles/account.scss";
import "@/styles/forms.scss";
import onLoad from "./onload.js";
import common from "./commonCode.js";
import filesize from "file-size";
import wrapPromise from "./wrapPromise.js";

const EMAIL_RE = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const USERNAME_RE = /^[a-zA-Z0-9_]{3,20}$/;

const statPromise = wrapPromise(() => client.getStats());
const dumpPromise = wrapPromise(() => client.dumpStatus());

// Make sure we tell superagent it's good to go
[statPromise, dumpPromise].forEach(p => p.then(r => r));

if (!Object.entries) {
  Object.entries = function(obj) {
    var ownProps = Object.keys(obj),
      i = ownProps.length,
      resArray = new Array(i); // preallocate the Array
    while (i--) resArray[i] = [ownProps[i], obj[ownProps[i]]];

    return resArray;
  };
}

onLoad(async function() {
  // TODO: lazy load the rest of this so we can work with responses as we get them (just domains and profile left to figure out since they're tricky)
  let errorBox = null;

  const email = document.getElementById("email");
  const username = document.getElementById("profile-username");
  const usernameInput = document.getElementById("username");
  const deleteForm = document.getElementById("delete-form");
  const deletePassword = document.getElementById("delete-password");
  const deleteBtn = document.getElementById("delete-btn");
  const deleteAccountBtn = document.getElementById("delete-modal-btn");
  const userId = document.getElementById("user-id");
  const revokePassword = document.getElementById("revoke-password");
  const revokeBtn = document.getElementById("revoke-btn");
  const stayLoggedIn = document.getElementById("stay-logged-in");
  const revokeForm = document.getElementById("revoke-form");
  const updateForm = document.getElementById("update-form");
  const submitBtn = document.getElementById("submit-btn");
  const newPassword = document.getElementById("new-password1");
  const password = document.getElementById("password");
  const newPassword2 = document.getElementById("new-password2");
  const totalShortens = document.getElementById("total-shortens");
  const totalDeleted = document.getElementById("total-deleted");
  const totalUsage = document.getElementById("total-usage");
  const totalFiles = document.getElementById("total-files");
  const profileQuota = document.getElementById("profile-quota");
  const profileUsed = document.getElementById("profile-used");
  const shortenProfileUsed = document.getElementById("s-profile-used");
  const shortenProfileQuota = document.getElementById("s-profile-quota");

  const queuePosition = document.getElementById("queue-position");
  const dumpWrap = document.getElementById("datadump");
  const fileNum = document.getElementById("file-num");
  const fileMax = document.getElementById("file-max");
  const dumpBtn = document.getElementById("request-data");

  async function renderDataDump(dumpStatus = null) {
    if (!dumpStatus) dumpStatus = await client.dumpStatus();
    if (dumpStatus.state == "not_in_queue") {
      dumpWrap.classList = "state-none";
    } else if (dumpStatus.state == "in_queue") {
      dumpWrap.classList = "state-requested";
      queuePosition.innerText = ordinal(dumpStatus.position);
    } else if (dumpStatus.state == "processing") {
      dumpWrap.classList = "state-processing";
      fileNum.innerText = dumpStatus.files_done;
      fileMax.innerText = dumpStatus.total_files;
    }
  }

  dumpPromise.then(dump => renderDataDump(dump));

  window.profilePromise.then(profile => {
    userId.innerText = `${client.token.substring(
      client.token.startsWith("u") ? 1 : 0,
      client.token.indexOf(".")
    )}`;
    username.innerText = profile.username;

    profileQuota.innerText = filesize(profile.limits.limit).human(); // / 1024 / 1024;
    profileUsed.innerText = filesize(profile.limits.used).human(); // Math.round(
    //   profile.limits.used / 1024 / 1024 || 0
    // );
    shortenProfileUsed.innerText = profile.limits.shortenused || 0;
    shortenProfileQuota.innerText = profile.limits.shortenlimit;

    if (profile.admin) {
      const adminBadge = document.createElement("span");
      adminBadge.innerText = "ADMIN";
      adminBadge.classList = "badge badge-pill badge-primary admin-badge";
      username.parentNode.appendChild(adminBadge);
    }
  });

  statPromise.then(stats => {
    totalShortens.innerText = stats.total_shortens;
    totalFiles.innerText = stats.total_files;
    totalDeleted.innerText = stats.total_deleted_files;
    totalUsage.innerText = filesize(stats.total_bytes).human();
  });

  updateForm.addEventListener("submit", function(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    return false;
  });

  dumpBtn.addEventListener("click", async function() {
    await client.requestDump();
    await renderDataDump();
  });

  revokeBtn.addEventListener("click", function() {
    revokeToken();
  });
  revokePassword.addEventListener("keydown", function(ev) {
    if (ev.key == "Enter") {
      revokeToken();
    }
  });

  deleteForm.addEventListener("submit", function(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    return false;
  });

  deleteBtn.addEventListener("click", function() {
    deleteAccount();
  });
  deletePassword.addEventListener("keydown", function(ev) {
    if (ev.key == "Enter") {
      deleteAccount();
    }
  });
  async function deleteAccount() {
    try {
      const emailToken = await client.deleteAccount(deletePassword.value);
      deleteAccountBtn.click();
    } catch (err) {
      deletePassword.setCustomValidity(err.userMessage || err.message);
      deleteForm.classList = "needs-validation was-validated";
    }
  }

  async function revokeToken() {
    try {
      await client.revokeTokens(revokePassword.value);
      if (stayLoggedIn.checked) {
        const token = await client.login(
          client.profile.username,
          revokePassword.value
        );
        window.localStorage.setItem("token", token);
        // Reload
        window.location.href = "";
        return;
      }
      window.location.pathname = "/";
    } catch (err) {
      revokePassword.setCustomValidity(err.userMessage || err.message);
      revokeForm.classList = "was-validated needs-validation";
      revokePassword.value = "";
      revokePassword.focus();
      return;
    }
  }

  await window.profilePromise;

  email.value = client.profile.email || "";
  usernameInput.value = client.profile.username || "";

  let emailVal = client.profile.email;
  let usernameVal = client.profile.username;
  submitBtn.addEventListener("click", async function() {
    common.handleErr();
    common.removeAlert(errorBox);
    let error = false;
    if (newPassword.value && newPassword2.value != newPassword.value) {
      newPassword2.setCustomValidity("Doesn't match!");
      error = true;
    } else newPassword2.setCustomValidity("");
    if (!email.value.match(EMAIL_RE)) {
      email.setCustomValidity("Invalid email!");
      error = true;
    } else email.setCustomValidity("");
    if (!usernameInput.value.match(USERNAME_RE)) {
      username.setCustomValidity("Invalid username!");
      error = true;
    } else email.setCustomValidity("");
    updateForm.classList = "was-validated needs-validation form-wrap";
    if (error) return;

    const modifications = {};

    if (newPassword.value) modifications.new_password = newPassword.value;

    if (usernameInput.value != usernameVal) {
      modifications.username = usernameInput.value;
    }

    if (email.value != emailVal) {
      modifications.email = email.value;
    }

    if (!password.value) {
      password.setCustomValidity("Invalid password!");
      error = true;
    } else password.setCustomValidity("");

    if (!Object.keys(modifications).length) return; // No changes to be made
    if (error) {
      updateForm.classList = "was-validated needs-validation form-wrap";
      return;
    }
    modifications.password = password.value || undefined;

    try {
      await client.updateAccount(modifications);
      errorBox = common.sendAlert("success", "Your changes have been saved!");

      if (modifications.email) emailVal = modifications.email;
      if (modifications.username) {
        username.innerText = usernameVal = modifications.username;
      }
    } catch (err) {
      if (err.errorCode == "BAD_AUTH") {
        password.setCustomValidity(err.userMessage);
        updateForm.classList = "was-validated needs-validation form-wrap";
      } else {
        common.handleErr(err);
      }
      return;
    }
    if (modifications.new_password) {
      window.localStorage.setItem("token", client.token);
      window.location.href = "";
    }
  });
});
