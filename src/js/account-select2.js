import $ from "jquery";
import "select2";
import "select2/dist/css/select2.css";
import onLoad from "./onload.js";
onLoad(function() {
  // Query strings are slow
  function officialDomainAdd(domain) {
    if (domain.id === undefined) return domain.text;
    const domainWrap = document.createElement("div");
    domainWrap.innerText = window.client.domains.domains[domain.id];
    if (window.client.domains.officialDomains.includes(Number(domain.id))) {
      domainWrap.classList = "official-domain";
      domainWrap.appendChild(officialTag.cloneNode(true));
    }
    return domainWrap;
  }
  $(document.getElementById("s-domain-selector")).select2({
    templateResult: officialDomainAdd
  });
  $(document.getElementById("domain-selector")).select2({
    templateResult: officialDomainAdd
  });
});

const officialTag = document.createElement("span");
officialTag.innerText = "OFFICIAL";
officialTag.classList = "badge badge-bill badge-success";
