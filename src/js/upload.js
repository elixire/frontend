// Stub
import commonCode from "./commonCode";
import "@/styles/upload.scss";
import filesize from "file-size";
import Clipboard from "clipboard";
import volumeUpIcon from "@/icons/speaker.svg";
import path from "path";
import superagent from "superagent";
import { renderTextProgress } from "./textProgress";
// Just a single pixel svg
const greenRect =
  "data:image/svg+xml;charset=utf-8;base64,PHN2ZyBoZWlnaHQ9IjEiIHdpZHRoPSIxIiB2aWV3Qm94PSIwIDAgMSAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxyZWN0IHdpZHRoPSIxIiBoZWlnaHQ9IjEiIHN0eWxlPSJmaWxsOiMzM2NjNGMiIC8+PC9zdmc+";
const redRect =
  "data:image/svg+xml;charset=utf-8;base64,PHN2ZyBoZWlnaHQ9IjEiIHdpZHRoPSIxIiB2aWV3Qm94PSIwIDAgMSAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxyZWN0IHdpZHRoPSIxIiBoZWlnaHQ9IjEiIHN0eWxlPSJmaWxsOiM5OTAwMDAiIC8+PC9zdmc+";
import onLoad from "./onload.js";

const oldTitle = document.title;

onLoad(function() {
  const uploadInput = document.getElementById("upload-input");
  const dropZone = document.body.parentElement;
  const dropCard = document.getElementById("dropper-card");
  const progressBar = document.getElementById("progress-bar");
  const savedFiles = document.getElementById("saved-files");
  const uploadText = document.getElementById("uploading-text");
  const uploadUrl = document.getElementById("upload-url");
  uploadUrl.addEventListener("keydown", async function(ev) {
    if (uploadUrl.disabled) return;
    uploadUrl.style.backgroundImage = "";
    uploadUrl.style.color = "";
    if (ev.key == "Enter") {
      uploadUrl.style.color = "#ffffff";
      uploadUrl.disabled = true;
      const request = superagent.get(uploadUrl.value).responseType("blob");
      uploadUrl.style.backgroundImage = `url('${greenRect}')`;
      uploadUrl.style.backgroundSize = "34px 0%";
      request.on("progress", function(progress) {
        if (
          progress.direction == "download" &&
          progress.percent !== undefined
        ) {
          uploadUrl.style.backgroundSize = `34px ${progress.percent || 0}%`;
          location.hash = renderTextProgress(progress.percent / 100, 20);
          document.title =
            renderTextProgress(progress.percent / 100, 5) + " — " + oldTitle;
        }
      });
      try {
        const res = await request;
        location.hash = "";
        document.title = oldTitle;
        var body = res.body;
        body.name = path.basename(uploadUrl.value);
      } catch (err) {
        uploadUrl.disabled = false;
        uploadUrl.style.backgroundImage = `url('${redRect}')`;
        uploadUrl.style.backgroundSize = "34px 100%";
        return;
      }
      await upload(body);
      uploadUrl.disabled = false;
      uploadUrl.value = "";
      uploadUrl.style.backgroundImage = "";
      uploadUrl.style.backgroundSize = "";
    }
  });
  document.addEventListener("paste", function(ev) {
    const items = Array.from(
      (ev.clipboardData || ev.originalEvent.clipboardData).items
    );
    const file = items.find(itm => itm.kind == "file");
    if (!file) return;
    ev.preventDefault();
    dropZone.classList = "show-popover";
    upload(file.getAsFile());
  });
  dropZone.addEventListener("dragover", function(ev) {
    if (Array.from(ev.dataTransfer.items).find(item => item.kind == "file")) {
      ev.preventDefault();
      dropZone.classList = "show-popover";
    }
  });
  dropZone.addEventListener("dragleave", function(ev) {
    if (ev.screenX == 0 && ev.screenY == 0) dropZone.classList = "";
  });
  dropZone.addEventListener("drop", function(ev) {
    ev.preventDefault();
    dropZone.classList = "show-popover";
    upload(ev.dataTransfer.files[0]);
  });
  uploadInput.addEventListener("change", () => {
    upload(uploadInput.files[0]);
  });
  async function upload(file) {
    if (!file) return;
    const fileSize = filesize(file.size).human();
    uploadText.innerText = `Uploading ${file.name} (${fileSize})`;
    commonCode.handleErr();
    dropZone.classList = "show-popover";
    const uploadReq = window.client.upload(file);
    uploadReq.on("progress", function(progress) {
      if (progress.direction == "upload") {
        progressBar.style.width = `${progress.percent || 0}%`;

        location.hash = renderTextProgress(progress.percent / 100, 20);

        document.title =
          renderTextProgress(progress.percent / 100, 5) + " — " + oldTitle;
      }
    });
    try {
      const url = await uploadReq
        .then(res => res.body.url)
        .catch(err => Promise.reject(window.client.handleErr(err)));
      location.hash = "";
      document.title = oldTitle;
      uploadText.innerText = "Drop to upload!";
      dropZone.classList = "";
      progressBar.style.width = "0";
      const newFile = document.createElement("div");
      const newFileLabel = document.createElement("span");
      const newFileSize = document.createElement("span");
      const newFileURL = document.createElement("a");
      const deleteFile = document.createElement("a");
      deleteFile.href = "#";
      deleteFile.classList = "delete-btn greyscale-icon";
      deleteFile.innerHTML = "&times;";
      deleteFile.addEventListener("click", async function() {
        commonCode.handleErr();
        try {
          const fileName = path.basename(url);
          await client.deleteFile(
            fileName.substring(0, fileName.lastIndexOf("."))
          );
          newFile.remove();
        } catch (err) {
          commonCode.handleErr(err);
        }
      });

      newFileURL.href = url;
      newFileURL.addEventListener("click", function(ev) {
        ev.preventDefault();
      });
      newFileURL.innerText = url;

      let newFileIcon = null;
      if (file.type.startsWith("video/")) {
        const imageBlob = new Blob([file], { type: file.type });
        const objectUrl = URL.createObjectURL(imageBlob);
        newFileIcon = document.createElement("video");
        newFileIcon.loop = true;
        newFileIcon.muted = true;
        newFileIcon.autoplay = true;
        const source = document.createElement("source");
        source.type = file.type;
        source.src = objectUrl;
        newFileIcon.appendChild(source);
        newFileIcon.addEventListener("loadeddata", async function() {
          try {
            await newFileIcon.play();
          } catch (err) {
            console.warn("Error playing preview", err);
          }
        });
      } else if (file.type.startsWith("audio/")) {
        newFileIcon = document.createElement("img");
        newFileIcon.src = volumeUpIcon;
      } else {
        const imageBlob = new Blob([file], { type: file.type });
        const objectUrl = URL.createObjectURL(imageBlob);
        newFileIcon = document.createElement("img");
        newFileIcon.src = objectUrl;
        newFileIcon.addEventListener("load", function() {
          URL.revokeObjectURL(objectUrl);
        });
      }
      newFileIcon.classList = "new-file-icon";
      newFileLabel.innerText = file.name;
      newFileSize.innerText = fileSize;
      newFile.classList = "saved-file";
      const clipboard = new Clipboard(newFileURL, {
        text: function() {
          return url;
        }
      });
      clipboard.on("success", function(ev) {
        const alertId = commonCode.sendAlert("success", "Copied to clipboard!");
        setTimeout(() => commonCode.removeAlert(alertId), 1500);
      });
      newFile.appendChild(newFileIcon);
      newFile.appendChild(newFileLabel);
      newFile.appendChild(newFileSize);
      newFile.appendChild(newFileURL);
      newFile.appendChild(deleteFile);
      savedFiles.appendChild(newFile);
      uploadInput.value = "";
    } catch (err) {
      uploadInput.value = "";
      uploadText.innerText = "Drop to upload!";
      dropZone.classList = "";
      progressBar.style.width = "0";

      commonCode.handleErr(err);
    }
  }
});
