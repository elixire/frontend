export default new Array(44)
  .fill(null)
  .map((a, i) => require(`arraybuffer-loader!@/images/momo/momo${i}.png`));

// export default require(`arraybuffer-loader!@/images/momo/momo*.png`);
