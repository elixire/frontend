// Stub
import commonCode from "./commonCode.js";
import "@/styles/forms.scss";
import onLoad from "./onload.js";

onLoad(async function() {
  const username = document.getElementById("username");
  const password = document.getElementById("password");
  const keepLogin = document.getElementById("keep-logged-in");
  const keepLoginBtn = document.getElementById("save-password");
  keepLogin.addEventListener("change", function() {
    if (!keepLogin.checked) {
      return;
    }
    keepLogin.checked = false;
    const btn = document.createElement("button");
    btn.setAttribute("data-toggle", "modal");
    btn.setAttribute("data-target", "#remember-modal");
    btn.classList = "btn btn-primary";
    btn.type = "button";
    btn.style.display = "none";
    document.body.appendChild(btn);
    btn.click();
  });
  keepLoginBtn.addEventListener("click", function() {
    keepLogin.checked = true;
  });
  const submit = document.getElementById("submit-btn");
  const resetModalBtn = document.getElementById("reset-modal-btn");
  username.focus();
  username.addEventListener("keydown", function(ev) {
    if (ev.key == "Enter") password.focus();
  });
  submit.addEventListener("click", login);
  password.addEventListener("keydown", function(ev) {
    if (ev.key == "Enter") login();
  });
  async function login() {
    commonCode.handleErr();
    try {
      const token = keepLogin.checked
        ? await commonCode.client.generateToken(password.value, username.value)
        : await commonCode.client.login(username.value, password.value);
      window.localStorage.setItem("token", token);
      window.location.pathname = window.location.hash.substring(1) || "/";
    } catch (err) {
      commonCode.handleErr(err);
    }
  }

  const resetForm = document.getElementById("reset-form");
  const resetUsername = document.getElementById("reset-username");
  resetForm.addEventListener("submit", function(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    return false;
  });
  const resetBtn = document.getElementById("reset-btn");
  resetBtn.addEventListener("click", async function() {
    try {
      await client.resetPassword(resetUsername.value);
      resetModalBtn.click();
    } catch (err) {
      console.log(err);
      resetUsername.setCustomValidity(err.userMessage || err.message);
      resetForm.classList = "was-validated needs-validation";
    }
  });

  const usernameForm = document.getElementById("username-form");
  const resetEmail = document.getElementById("email-username");
  const recoverModalBtn = document.getElementById("username-modal-btn");
  usernameForm.addEventListener("submit", function(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    return false;
  });
  const recoverBtn = document.getElementById("username-btn");
  recoverBtn.addEventListener("click", async function() {
    try {
      await client.recoverUsername(resetEmail.value);
      recoverModalBtn.click();
    } catch (err) {
      console.log(err);
      resetEmail.setCustomValidity(err.userMessage || err.message);
      usernameForm.classList = "was-validated needs-validation";
    }
  });
});
