export function renderTextProgress(percent, blockCount = 20) {
  const scaler = ["⠀", "▏", "▎", "▍", "▌", "▋", "▊", "▉"];
  const done = percent * blockCount;
  const base = Math.floor(done);
  const progressBar =
    "[" +
    (
      scaler[scaler.length - 1].repeat(base) +
      // We do scaler.length -1 because otherwise we're putting ourselves under a bad scale, as whole num=scaler.length
      scaler[Math.floor((done - base) * (scaler.length - 1))]
    ).padEnd(blockCount, scaler[0]) +
    "]" +
    (Math.floor(percent * 100) + "%").padStart(5, scaler[0]);
  return progressBar;
}
